//<!--
//-TriangleSolver.cs
//description: this is the Triangle Solver class. this class is used to analyze the numbers that the user inputs to determine what type of 
//triangle the user has created, or not created
//-Professor: Rob Bowyer
//-Created by: Chackaphope Ying Yong 8042814
//-Created: 2020.02.26
//-Last Revised: 2020.02.28
//-->
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CYingYongAssignment2
{
    public static class TriangleSolver
    {
        public static int StaticA { get; set; }
        public static int StaticB { get; set; }
        public static int StaticC { get; set; }

        public static bool isTriangle { get; set; }
        public static bool isEqualateral { get; set; } = false;

        public static bool isScalene { get; set; } = false;
        public static bool isIsoscelse { get; set; } = false;

        public static bool has0 { get; set; } = false;


        //this method will analyze the 3 integers and determine if the  triangle is formed or not, and which type of triangle is formed
        public static void Analyze(int StaticA, int StaticB, int StaticC)
        {
            //these are local variables used for determining if the dimensions form a triangle
            int AB = StaticA + StaticB;
            int BC = StaticB + StaticC;
            int CA = StaticC + StaticA;
            isTriangle = false;

            //this is the if statement that determines if the triangle is an equalateral
            if (StaticA == StaticB && StaticB == StaticC)
            {
                isTriangle = true;
                isEqualateral = true;
                Console.WriteLine("You have formed an equilateral triangle");
                Console.WriteLine("Press enter to go back to main menu");
            }

            //this is the statement that handles if the user had inputed a 0 as a dimension
            else if (StaticA == 0 || StaticB == 0 || StaticC == 0)
            {
                isTriangle = false;
                has0 = true;
                Console.WriteLine("Cannot form a triangle with a side with length 0");
                Console.WriteLine("Press enter to go back to main menu");
            }

            //this is the statement that determines if the lengths of the sides are in range to form a triangle, if a side is too big/small
            //then the user will be noted that the input does nnot form a triangle
            else if (AB < StaticC || BC < StaticA || CA < StaticB)
            {
                isTriangle = false;
                Console.WriteLine("Cannot form a triangle with these dimensions");
                Console.WriteLine("Press enter to go back to main menu");
            }

            //this statement determines if the user has inputed a scalene dimension for a triangle, a triangle dimensions are in proportion
            //to form this triangle
            else if (StaticA != StaticB && StaticA != StaticC)
            {
                isTriangle = true;
                isScalene = true;
                Console.WriteLine("You have formed an scalene triangle");
                Console.WriteLine("Press enter to go back to main menu");
            }

            //this is the statement that determines if the user has inputed an isosceles dimesion 
            else if (StaticA == StaticB || StaticA == StaticC || StaticB == StaticC)
            {
                isTriangle = true;
                isIsoscelse = true;
                Console.WriteLine("You have formed an isoscelse triangle");
                Console.WriteLine("Press enter to go back to main menu");
            }
        }
    }
}
