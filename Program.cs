//<!--
//-program.cs
//discription: This is the program file which contains the main method. This program is suppose to have the user input 3 triangles, and the program
//will respond telling the user if the triangle is an equalateral, scalene, or isosceles triangle, or if the dimensions do not form a triangle
//-Professor: Rob Bowyer
//-Created by: Chackaphope Ying Yong 8042814
//-Created: 2020.02.26
//-Last Revised: 2020.02.28
//-->
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CYingYongAssignment2
{
    class Program
    {
        static void Main(string[] args)
        {
            string input;
            int a, b, c;

            //this do loop will run unless the user inputs a 2
            do
            {
                Console.WriteLine("1. Please enter 3 numbers. Press enter after each number please, and nothng but numbers.");
                Console.WriteLine("2. Exit");
                input = Console.ReadLine();


                //if the user inputs 1, the user will be asked to input 3 numbers for a triangle dimension. The numbers/intergers are then stored within
                //the static class TriangleSolver. In the end of the code, the 3 numbers will be analyzed to determine wheathe the triangle
                //is formed or not, or which type of triangle is it
                if (input == "1")
                {
                    //the try method will catch will ensure that the user only inputs a number for the triangle dimensions
                    try 
                    { 
                        Console.WriteLine("Please input the length of the first side of the triangle");
                        a = Convert.ToInt32(Console.ReadLine());
                        TriangleSolver.StaticA = a;              //these integers are stored in the static class which analyzes the triangle
                        Console.WriteLine("Please input the length of the second side of the triangle");
                        b = Convert.ToInt32(Console.ReadLine());
                        TriangleSolver.StaticB = b;
                        Console.WriteLine("Please input the length of the third side of the triangle");
                        c = Convert.ToInt32(Console.ReadLine());
                        TriangleSolver.StaticC = c;
                        TriangleSolver.Analyze(a, b, c); //this method comes from the TriangleSolver class
                        Console.ReadLine();
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Please only input numbers.");
                        Console.WriteLine();
                    }
                }
                if (input == "2")  //inputing 2 will close the application
                {
                    Console.WriteLine("Press any key to close application");
                    Console.ReadKey();
                }
            }
            while (input != "2");
        
    }
}
