This is a the README file for my assignment 3; it will tell the user how to open and run the program.

The program is a triangle solver, the user will input 3 integers, then the program will determine either if the triangle
is possible to make, or which type of triangle it had made.

How to run:
To run the program you have to run both files in the same filepath in visual studio. The program is the main file (program.cs)
and TrangleSolver.cs is an additional class implemented in the main program.

You must have both files in the same pathway, and then when you are at visual studio, just press run and the
program will run a console application. 
